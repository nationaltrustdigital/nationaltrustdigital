/**
	 * This module is responsible for binding functionality to the search form
	 */
(function (NtGeocode, $) {

  //configure autosuggest
  $('#search-query').nt_autosuggestPlaces({
    url: [['/autosuggest']]
  });

  // if we have an exact match, go to that URL
  var goToPlaceIfExactMatch = function () {
    var searchType = $("#search-form-type").val();
    if (searchType==="place" && $.fn.nt_autosuggestPlaces && $.fn.nt_autosuggestPlaces.exactMatchUrl && $.fn.nt_autosuggestPlaces.exactMatchUrl !== "") {
    	
      document.cookie = "fromSearchBox="+$.fn.nt_autosuggestPlaces.exactMatchName+
      ","+window.location.pathname+"; expires="+new Date(new Date().getTime()+60000).toGMTString();
    	
      window.location.href = $.fn.nt_autosuggestPlaces.exactMatchUrl;
      return true;
    }
    return false;
  };

  //process a result from geocode lookup to extract lat and lon
  var handleGeocodeResult = function (query, latitude, longitude) {
    if (query.length === 0) {
      return;
    }
    // set hidden form fields
    if (latitude && longitude) {
      $("#search-form-lat").val(latitude);
      $("#search-form-lon").val(longitude);
    } else {
      $("#search-form-lat").remove();
      $("#search-form-lon").remove();
    }

    // submit the form
    var searchForm = $("#search-form");
    searchForm.unbind("submit");
    searchForm.submit();
  };

  $('#search-query').focus( 
    function() {
    	if (navigator.geolocation) {
    		$('#nt-location-search').addClass("nt-show-geolocation");
    	}
    }
  );

  $('#search-query').blur( 
    function() {
    	if (navigator.geolocation) {
    		setTimeout( function() {$('#nt-location-search').removeClass("nt-show-geolocation"); },1000 );
    	}
    }
  );

  $("#search-getUsersLocation").click(function (event) {
    NtGeocode.getUsersLocation(
      function(geoloaction) {
        $("#search-form-lat").val(geoloaction.latitude);
        $("#search-form-lon").val(geoloaction.longitude);
        $("#search-query").val("My location"+geoloaction.name);
        $("#search-form").submit();
      }, // ok
      function(message) {
        $("#validationMessage").text("We can’t find you as your location services are disabled. Please enable location services, or enter a location in the search above");
      },// blocked
      function(message,code) {
        $("#validationMessage").text("We can’t seem to find you. Please try again shortly or enter a location in the search above");
      } // error
    );
    return false;
  });

  // initial binding of the search form submission to capture the search so we can perform a geocode lookup first
  $("#search-form").submit(function (event) {
    var query = $("#search-query").val();

    //We might be able to shortcut this if the autocomplete gave us a url
    if( goToPlaceIfExactMatch()===false ) {
      if(query.indexOf("My location")!==0) {
    	//We are not using the users location
        //We have not got an exact match, go a geocode lookup
        NtGeocode.geocode(query, handleGeocodeResult);
        event.preventDefault();
      }
  	  //We are using the users location. Submit the form
    } else {
        event.preventDefault();
    }
  });

}(NtGeocode, jQuery));

