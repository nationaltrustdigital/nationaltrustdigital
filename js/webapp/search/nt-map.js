var NtMap = function () {

  var CLUSTER_PIN_ICON = "/assets/img/44-location-searched-purple.png";
  var DEFAULT_PIN_ICON = "/assets/img/43-location-pin-pink.png";
  var SELECTED_PIN_ICON = "/assets/img/43-location-pin-blue.png";
  var MAP_LOCATION_ICON = "/assets/img/location-searched.png";
  var CLOSE_ICON_URL = "/assets/img/nt-close-card-bk.png";
  var eventSearchMapAnalyticsCode = 'EventMapClick';
  var placeSearchMapAnalyticsCode = 'PlaceMapClick';
  var lastInfoWindow=null;
  var map = null;
  var smallScreen = false;
  var allPlaces = {};
  var self = this;
  var mapReadyTimeout;
  var bounceTimeout;
  var displayWhatsOn = false;
  var filteredSearch = false;
  var markerClusterer;
  var lastMarker;

  //Create an info window for a place with content
  var setInfoWindow = function(place,content) {
    var infoOptions = {
      position: new google.maps.LatLng(place.location.latitude, place.location.longitude),
      pixelOffset: new google.maps.Size(-200, -60),
      infoBoxClearance: new google.maps.Size(1, 1),
      isHidden: false,
      pane: "floatPane",
      alignBottom: true,
      disableAutoPan: false,
      enableEventPropagation: false,
      closeBoxMargin: "2px 2px 2px 2px",
      closeBoxURL: CLOSE_ICON_URL,
      content: content,
      boxStyle: {
        width: "100%",
        maxWidth: "400px",
        backgroundColor: "white"
      }
    };
    var infoWindow = new InfoBox(
      infoOptions
    );
    // Set infoWindow to open on clicking on place
    place.marker.addListener('click',
                             function() { 
                               if(lastInfoWindow!==null) {
                                 lastInfoWindow.close();
                               }

                               infoWindow.open(map,place.marker);
                               place.marker.setAnimation(google.maps.Animation.BOUNCE);
                               bounceTimeout = setTimeout( function() {place.marker.setAnimation( null );},2800 );
                               lastInfoWindow = infoWindow;
                               map.panTo(place.marker.getPosition());
                             }
                            );

  };

  var getPlacePageUrl = function (place) {
    return place.websiteUrl;
  };

  var trimTrailingSlash = function (url) {
    return (url[url.length-1] === '/') ? url.substr(0,url.length-1) : url;
  };

  var getEventsPageUrl = function (place) {
    return trimTrailingSlash(place.websiteUrl) + '/whats-on';
  };

  // converts a javascript map into array of individual query string params URL encoded 
  // e.g. The following input: 
  //
  // {'key1': 'abc', 'key2': 'def', 'key3': 'g h i'}
  // 
  // returns
  // 
  // ["key1=abc", "key2=def", "key3=g%20h%20i"]
  var mapToQueryStringParams = function (queryStringMap) {
    return $.map(queryStringMap, function (value, key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(value);
    });
  };

  // append query string parameters into a single query string
  // e.g. 
  // Input
  // 
  // ["key1=abc", "key2=def", "key3=g%20h%20i"]
  //
  // Output: 
  // 
  // &key1=abc&key2=def&key3=g%20h%20i
  var concatenateQueryStringParams = function (queryStringParams) {
    var queryString = '';
    $.each(queryStringParams, function (i, value) {
      queryString += '&' + value;
    });
    return queryString;
  };

  // given a url, and an object map of query string parameters and corresponding values
  // returns a new url that has the query string parameters appended
  // e.g. 
  // url: http://www.nationaltrust.org.uk/places/killerton
  // queryStringMap: {'EventMapClick', 'Killerton'}
  //
  // returns: http://www.nationaltrust.org.uk/places/killerton?EventMapClick=Killerton
  var augmentUrlWithAdditionalQueryStringParams = function (url, queryStringMap) {
    // anchor tag converts the URL into a Location object
    var a = document.createElement('a'),
        queryString = concatenateQueryStringParams(mapToQueryStringParams(queryStringMap));
    a.href = url;

    // append query string params
    if (queryString !== '') {
      if (a.search === '') {
        a.search = queryString.substring(1);
      } else {
        a.search = queryString;
      }
    }

    return a.toString();
  };

  // Add the appropriate place/event search analytic code to the URL's query string
  var getAugmentedUrlWithAnalyticsCode = function (url, place) {
    // the analytic code depends upon the type of map displayed on the page
    var queryStringMap = {},
        analyticsCode = displayWhatsOn ? eventSearchMapAnalyticsCode : placeSearchMapAnalyticsCode;

    queryStringMap[analyticsCode] = place.id;

    return augmentUrlWithAdditionalQueryStringParams(url, queryStringMap);
  };

  //Generate infoWindow content for a place
  var getContent = function(place) {
    var title = place.title;
    var subTitle = place.subTitle;
    var description = place.description;
    var imageUrl = place.imageUrl;
    var imageDescription = place.imageDescription;
    var placePageUrl = getAugmentedUrlWithAnalyticsCode(getPlacePageUrl(place), place);
    var eventsPageUrl = getAugmentedUrlWithAnalyticsCode(getEventsPageUrl(place), place);
    var responsiveImageServer = $('script[data-nt-resrc-server]').data('nt-resrc-server');

    var htmlString = '<a href="' + placePageUrl + '" style="display:block;border-bottom-style:none;">';
    htmlString += '<div class="card nt-masonry-single-result" id="place-card">';
    htmlString += '<div class="card-inner nt-masonry-single-result-inner">';
    if(!smallScreen) {
      htmlString += '<div class="image nt-masonry-single-result-image nt-image-wrap nt-image-wrap-16x9">';
      htmlString += '<img src="//' + responsiveImageServer + '/s=w400,pd1/c=ar16x9/';
      htmlString += imageUrl;
      htmlString += '" alt="'+ imageDescription +'">';
      htmlString += '</div>';
    }
    htmlString += '<div class="content nt-masonry-single-result-inner map-card-inner">';
    htmlString += '<h3 class="card-title nt-link-chevron">';
    htmlString += '<a href="';
    htmlString += placePageUrl;
    htmlString += '">';
    htmlString += title;
    htmlString += '</a>';
    htmlString += '</h3>';
    htmlString += '<div class="nt-masonry-single-result-category">'+ subTitle +'</div>';
    htmlString += '<p>';
    htmlString += description;
    htmlString += '</p>';
    if(displayWhatsOn && place.selected) {
      htmlString += '<a href="' + eventsPageUrl + '">';
      htmlString += '<div class="nt-search-whats-on-result-event-link nt-palette-neutral nt-palette-hover nt-js-link nt-link-chevron">';
      htmlString += 'View all events&nbsp;';
      htmlString += '</div>';
      htmlString += '</a>';
    }
    htmlString += '</div>';
    htmlString += '</div>';
    htmlString += '</div>';
    htmlString += '</a>';
    return htmlString;
  };

  //Apply marker clustering
  var clusterMarkers = function(markers) {
    markerClusterer = new MarkerClusterer( map, markers, 
                                          {
                                            gridSize: 50,
                                            maxZoom: 6,
                                            styles: [{
                                              url: CLUSTER_PIN_ICON,
                                              height: 32,
                                              width: 32,
                                              textColor: "white"
                                            }]
                                          }
                                         );
  };

  self.init = function(places, canvas) {
    nt_loadLibraries([
      {src: "/assets/js/webapp/search/markerclusterer-1.0.js", path: "MarkerClusterer"},
      {src: "/assets/js/webapp/search/infobox-1.1.13.js", path: "InfoBox"},
      //FIXME: We wait for google to load but do not load it ourselves as it is currently loaded by nt-search-bootstrap
      //This will be reviewed as part of the overall main.js review/splitting out.
      {path: "google"}
    ], function() {initMap(places, canvas);});
  };

  self.resize = function() {
    if(!mapReady) {
      mapReadyTimeout = setTimeout( function() {self.resize(); },250 );
      return;
    }
    google.maps.event.trigger(map, "resize");
  };

  self.setSmallScreen = function(state) {
    smallScreen = state;
  };

  self.setDraggable = function(state) {
    if(!mapReady) {
      mapReadyTimeout = setTimeout( function() {self.setDraggable(state); },250 );
      return;
    }
    map.setOptions( {draggable: state} );
  };

    self.setScrollWheel = function (state) {
        if (!mapReady) {
            mapReadyTimeout = setTimeout(function () {
                self.setScrollWheel(state);
            }, 250);
            return;
        }
        map.setOptions({scrollwheel: state});
    };

  self.setDisplayWhatsOn = function(state) {
    displayWhatsOn = state;
  };

  self.setFilteredSearch = function(state) {
    filteredSearch = state;
  };

  var mapReady = false;
  var initMap = function(places,canvas) {
    allPlaces = {};
    if(window.matchMedia) {
      if( window.matchMedia( "screen and (max-device-height: 599px)" ).matches ) {
        smallScreen = true;
      }
    }

    if (typeof(nt_eventResults) !== 'undefined' && nt_eventResults === true) {
      self.setDisplayWhatsOn(true);
    } else {
      self.setDisplayWhatsOn(false);
    }
    if (typeof(nt_isFilteredSearch) !== 'undefined' && nt_isFilteredSearch === true) {
      self.setFilteredSearch(true);
    } else {
      self.setFilteredSearch(false);
    }

    for(var placeId in places) {
      var place = places[placeId];
      if( typeof(place.location)!=="undefined" && place.location!==null && typeof(place.location.latitude)!=="undefined" ) {
        allPlaces[placeId] = places[placeId];
      }
    }
    var markers = [];

    //Create the google map
    map = new google.maps.Map(
      canvas,
      {
        center: { lat: 53.93, lng: -2.33},
        zoom: 6
      }
    );

    map.addListener("click", function(location) {
      if(lastInfoWindow!==null) {
        lastInfoWindow.close();
      }
    });

    //Create a pin for each place and add an info window
    //Associate the pin marker with the place object
    for(var placeId in allPlaces) {
      var place = allPlaces[placeId];

      place.marker = new google.maps.Marker({
        map: map,
        icon: DEFAULT_PIN_ICON,
        title: place.title,
        position: {lat: place.location.latitude, lng: place.location.longitude}
      });

      markers.push( place.marker );
    }

    //Apply marker clustering
    clusterMarkers( markers );

    mapReady = true;
  };

  self.centerOnPlace = function(placeId) {
    if(!mapReady) {
      mapReadyTimeout = setTimeout( function() {self.centerOnPlace(placeId);},250 );
      return;
    }

    var place = allPlaces[placeId];
    self.displayResults([placeId], place.location);
  };

  //Display a list of place ids on the map and set map to show the set of places
  self.displayResults = function(placeIds,mapCenter) {
    if(!mapReady) {
      mapReadyTimeout = setTimeout( function() {self.displayResults(placeIds,mapCenter);},250 );
      return;
    }

    var place;
    var placeId;
    var bounds = new google.maps.LatLngBounds();
    var filterDisplayedPlaces = displayWhatsOn || filteredSearch;

    //Set all places to use the default pin
    for(placeId in allPlaces) {
      place = allPlaces[ placeId ];
      place.marker.setIcon( DEFAULT_PIN_ICON );
      place.selected = false;

      if (filterDisplayedPlaces){
        place.marker.setVisible(false);
      }
    }
    if (filterDisplayedPlaces){
      markerClusterer.clearMarkers();
    }

    //Set selected places to use the selected pin
    for(var i =0; i<placeIds.length ; i++) {
      placeId = placeIds[i];
      place = allPlaces[placeId];

      if( typeof(place)!=="undefined" ) {

        if ($('#places-nearby-map').length){
          place.marker.setIcon( SELECTED_PIN_ICON );
        }
        if (filterDisplayedPlaces){
          place.marker.setVisible(true);
          markerClusterer.addMarker(place.marker);
        }
        place.selected = true;
        //Extend bounds to include this pin
        bounds.extend( new google.maps.LatLng(place.location.latitude,place.location.longitude) );
      }
    }

    //Set all place infoWindow content
    for(placeId in allPlaces) {   
      place = allPlaces[ placeId ];
      setInfoWindow( place,getContent(place) );
    }

    if( typeof(mapCenter)!=="undefined" && mapCenter!==null ) {
      //Center map
      map.setCenter(new google.maps.LatLng(mapCenter.latitude,mapCenter.longitude));
      map.setZoom(10);
    } else {
      //Apply bounds to map
      map.fitBounds( bounds );
    }
  };

  self.placeMarker = function(position,title,centerOnPosition) {
    if(!mapReady) {
      mapReadyTimeout = setTimeout( function() {self.placeMarker(position,title,centerOnPosition); },250 );
      return;
    }

    lastMmarker = new google.maps.Marker({
      position: position,
      map: map,
      title: title,
      icon: MAP_LOCATION_ICON
    });
    if(centerOnPosition) {
      map.setCenter(new google.maps.LatLng(position.lat,position.lng));
    }
  };

  self.getLastMarker = function() {
    return lastMarker;
  };

  self.getMap = function(){
    return map;
  };

  self.allPlaces = function(){
    return allPlaces;
  };

};
