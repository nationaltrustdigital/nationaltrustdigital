var nt_mapReady = false;
var ntMap = null;
var keep;

nt_loadLibraries([{path: "jQuery.fn.nt_resize"}], function() {

  var calculateMapCanvasHeight = function () {
    return $("#modal-map").find(".nt-modal-panel").height() - $("#map-header").height();
  };

  var calculateMapCanvasMarginTop = function () {
	  return $("#map-header").height();
  };

  var resizeMapCanvas = function () {
	  var canvas = $("#map-canvas");
	  canvas.height(calculateMapCanvasHeight());
	  if(!navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
		  canvas.css('margin-top', calculateMapCanvasMarginTop());
	  }
  };

  jQuery(document).on('click.modal', function () {
    if (nt_mapReady) {
      if (ntMap!==null) {
        ntMap.resize();
      }
      return;
    }
  });
  jQuery('map-container').on('shown', function () {
    if (nt_mapReady) {
      if (ntMap!==null) {
        ntMap.resize();
      }
      return;
    }
  });
  jQuery("#modal-map-button").on('click', function () {
    keep = $("#search-result-cards").html();
    $("#search-result-cards").html("");

    nt_mapReady = true;
    jQuery.getJSON("/search/data/all-places","",
                   function(response) {
                     var mapCanvas = document.getElementById("map-canvas");

					 resizeMapCanvas();

                     ntMap = new NtMap();
                     ntMap.init(response,mapCanvas);
                     ntMap.displayResults(nt_searchResultsPlaceIds,nt_searchGeolocation);
                     ntMap.resize();

                     if($("#search-query").val().indexOf("My location")===0) {
                       var latitude = parseFloat( $("#search-form-lat").val() );
                       var longitude = parseFloat( $("#search-form-lon").val() );
                       ntMap.placeMarker({lat:latitude,lng:longitude},$("#search-query").val(),true);
                     }
                   });
  });
  jQuery(window).nt_resize({throttleDelay: 300, endDelay: 300});

  jQuery(window).on('nt-resize nt-resize-end', function(event) {
	resizeMapCanvas();
    if (nt_mapReady) {
      if (ntMap!==null) {
        ntMap.resize();
        ntMap.displayResults(nt_searchResultsPlaceIds,nt_searchGeolocation);
      }
    }
  });
});

function showSearchResults() {
  $("#search-result-cards").html(keep);
}
