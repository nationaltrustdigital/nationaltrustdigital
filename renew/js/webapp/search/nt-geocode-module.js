/**
 * NTGeocode is a wrapper around googles geocoder
 *
 *
 *
 * @type {{geocode}}
 */
var NtGeocode = (function (google) {

  var googleGeocoder = new google.maps.Geocoder();

  /**
	 * geocode function takes a string query to be resolved to a lat lon
	 *
	 * @param query
	 * @param callback function(query,lat,lon)
	 *        where query will be the original query string and lat and lon may be null if nothing is found
	 */
  var geocode = function (query, callback) {
    jQuery.getJSON("/search/data/custom-locations?query="+query,
                   function( result ) {
                     if(result.location) {
                       callback(query, result.location.latitude, result.location.longitude);
                     } else {
                       googleGeocode(query, callback);
                     }
                   }
                  );
  };

  var googleGeocode = function (query, callback) {

    googleGeocoder.geocode({
      'address': query,
      'componentRestrictions': {
        country: 'UK'
      }
    }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        results = filterResults(results);

        if (results.length > 0) {
          callback(query, results[0].geometry.location.lat(),
                   results[0].geometry.location.lng());
        } else {
          callback(query);
        }
      } else {
        callback(query);
      }
    });
  };

  var filterResults = function (results) {
    var filteredResults = [];
    for (var resultIndex in results) {
      var result = results[resultIndex];

      if (isIncludedGoogleLocation(result.types)) {
        filteredResults.push(result);
      }

    }

    return filteredResults;
  };

  var isIncludedGoogleLocation = function (types) {
    var isPointOfInterest = false;
    var isEstablishment = false;
    for (var typeIndex in types) {
      var type = types[typeIndex];
      if (type === "locality" || type === "postal_code") {
        return true;
      } else if (type === "establishment") {
        isEstablishment = true;
      } else if (type === "point_of_interest" || type === "natural_feature") {
        isPointOfInterest = true;
      }
    }
    if (isEstablishment && isPointOfInterest) {
      return true;
    } else {
      return false;
    }
  };

	var getUsersLocation = function (callback, onBlocked, onError) {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				function (position) {
					var geoloaction = {
						latitude: position.coords.latitude,
						longitude: position.coords.longitude
					};

					nameUsersLocation(geoloaction, callback);
				},
				function (error) {
					if (error.code === error.PERMISSION_DENIED) {
						if (typeof onBlocked !== 'undefined') {
							onBlocked(error.message);
						}
					} else {
						if (typeof onError !== 'undefined') {
							onError(error.message, error.code);
						}
					}
				},
				{
					enableHighAccuracy: false
				}
			);
		}
	};

  var nameUsersLocation = function(position,callback) {
    var latlng = {lat: position.latitude, lng: position.longitude};

    googleGeocoder.geocode( { 'location': latlng }, function(position,callback) {
      return function(results, status) {
        position.name = "";
        if (status == google.maps.GeocoderStatus.OK) {
        	var location = results[0].formatted_address;
        	//Split into address parts
        	var parts = location.split(",");
        	var address = "";
        	var i = 0;
        	//Remove all space padding
        	for(i=1;i<parts.length;i++) {
        	  parts[i] = parts[i].trim();
        	}
        	
        	//Rebuild address ignoring repeated address elements
        	for(i=1;i<parts.length;i++) {
        	  if(i+1<parts.length && parts[i+1].indexOf(parts[i])===0) {
        	      continue;
        	  } else {
        	    if(address.length>0) {
        	      address+=", ";
        	    }
        	    address+=parts[i];
        	  }
        	}
        	
        	position.name = ", "+address;
        }
        callback(position);
      };
    }(position,callback)
    );
  };

  return {
    "geocode": geocode,
    "getUsersLocation": getUsersLocation
  };

})(google);
