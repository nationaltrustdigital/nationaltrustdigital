(function( $ ) { 
    $.fn.nt_placeOpeningTimesCalendar = function(params) {   	
    	return this.each(function() {
	    	f = buildEventHandler(params);
	    	$(this).click(f);
    	});
    };
    
    function buildEventHandler(params) {    	
    	/*
    	 * The function arguments (params) are cached in this closure.
    	 * params is the optional parameters object passed into the plugin as per plugin standards.
    	 * All events using this function use the same closure, and therefore the same params.
    	 */
    	var f = function (event) {
    		var target = $(event.target);
    		/*
    		 * if the click target was the anchor tag's inner span (if the user clicked directly on the day of month in the cell corner),
    		 * make the anchor tag the click target. The anchor has class nt-calendar-modal-selectable.
    		 * If this is a non selectable cell with no anchor tag, we will either end up on an inner span or the <td>,
    		 * in which case the if test below will weed it out as it won't have class nt-calendar-modal-selectable.
    		 */
    		target = target.hasClass("nt-calendar-modal-selectable") ? target : target.parent();
    		
    		if (target.hasClass("nt-calendar-modal-selectable")) {
		    	$("main tr.nt-opening-times-calendar-day-details").hide();
		    	expandedCellReclicked = target.parent().hasClass("calendar-modal-active");
		    	$("main td.nt-calendar-modal-day-cell").removeClass("calendar-modal-active calendar-modal-expanded");
		    	
		    	if (!expandedCellReclicked) {
			    	target.parent().addClass("calendar-modal-active calendar-modal-expanded");
			    	$("#nt-opening-times-data-"+target.attr("data-nt-day")).show();
		    	}
		    }
    	};
    	return f;
    }
}( jQuery ));

$('#nt-opening-times-calendar').nt_placeOpeningTimesCalendar({});
